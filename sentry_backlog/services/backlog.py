import requests


class ApiV2:

    METHOD = None
    URL = None

    def __init__(self, space_key, api_key):
        self.space_key = space_key
        self.api_key = api_key

    def _url_format(self, **url_params):
        path_format = '/'.join([
            '{' + path[1:] + '}' if path.startswith(':') else path
            for path in self.URL.split('/')
        ])
        path = path_format.format(**url_params)

        request_url = 'https://{space_key}.backlog.jp{path}'.format(
            space_key=self.space_key, path=path)

        return request_url

    def send(self, payload=None, url_params=None):
        if payload is None:
            payload = {}
        if url_params is None:
            url_params = {}

        request_url = self._url_format(**url_params)

        if self.METHOD == 'GET':
            payload['apiKey'] = self.api_key
            response = requests.get(request_url, params=payload)
        elif self.METHOD == 'POST':
            response = requests.post(
                request_url, data=payload, params={'apiKey': self.api_key})
        else:
            raise ValueError('Unknown method: {0}'.format(self.METHOD))

        if response.ok is False:
            raise RuntimeError

        return response.json()


class SpaceInfo(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/space'


class PriorityList(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/priorities'


class ProjectList(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/projects'


class ProjectInfo(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/projects/:projectIdOrKey'


class CategoryList(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/projects/:projectIdOrKey/categories'


class IssueTypeList(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/projects/:projectIdOrKey/issueTypes'


class UserList(ApiV2):

    METHOD = 'GET'
    URL = '/api/v2/projects/:projectIdOrKey/users'


class CreateIssue(ApiV2):

    METHOD = 'POST'
    URL = '/api/v2/issues'
