from flask import (
    Blueprint,
    render_template,
)
from jinja2 import PackageLoader


app = Blueprint('index', __name__)
app.jinja_loader = PackageLoader('sentry_backlog')


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')
