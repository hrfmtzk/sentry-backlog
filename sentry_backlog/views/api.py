from flask import (
    Blueprint,
    request,
)
from flask_restful import (
    Api,
    Resource,
    reqparse,
)

from sentry_backlog.adapters.backlog import IssueAdapter
from sentry_backlog.services.backlog import (
    CategoryList,
    CreateIssue,
    IssueTypeList,
    PriorityList,
    ProjectInfo,
    ProjectList,
    UserList,
)

app = Blueprint('api', __name__, url_prefix='/api')
api = Api(app)


parser = reqparse.RequestParser()
parser.add_argument('api_key', required=True, help='API key')


def project_key_to_id(api_key, space_key, project_key):
    url_params = {
        'projectIdOrKey': project_key,
    }
    result = ProjectInfo(space_key, api_key).send(url_params=url_params)

    return result['id']


class Index(Resource):

    def get(self):
        return {'I am': 'index'}


class Priority(Resource):

    def get(self, space_key):
        args = parser.parse_args()
        try:
            priorities = PriorityList(space_key, args.api_key).send()
        except:
            return dict(result=False)
        return dict(result=True, priorities=priorities)


class Project(Resource):

    def get(self, space_key):
        args = parser.parse_args()
        try:
            projects = ProjectList(space_key, args.api_key).send()
        except:
            return dict(result=False)
        return dict(result=True, projects=projects)


class Category(Resource):

    def get(self, space_key, project_key):
        args = parser.parse_args()
        url_params = {
            'projectIdOrKey': project_key,
        }
        try:
            categories = CategoryList(
                space_key, args.api_key
            ).send(url_params=url_params)
        except:
            return dict(result=False)
        return dict(result=True, categories=categories)


class IssueType(Resource):

    def get(self, space_key, project_key):
        args = parser.parse_args()
        url_params = {
            'projectIdOrKey': project_key,
        }
        try:
            issue_types = IssueTypeList(
                space_key, args.api_key
            ).send(url_params=url_params)
        except:
            return dict(result=False)
        return dict(result=True, issueTypes=issue_types)


class User(Resource):

    def get(self, space_key, project_key):
        args = parser.parse_args()
        url_params = {
            'projectIdOrKey': project_key,
        }
        try:
            users = UserList(
                space_key, args.api_key
            ).send(url_params=url_params)
        except:
            return dict(result=False)
        return dict(result=True, users=users)


class Issue(Resource):

    def post(self, space_key, project_key):
        issue_parser = parser.copy()
        issue_parser.add_argument(
            'issue_type_id',
            type=int,
            location='args',
            required=True,
        )
        issue_parser.add_argument(
            'priority_id',
            type=int,
            location='args',
            required=True,
        )
        issue_parser.add_argument(
            'category_ids',
            type=int,
            action='append',
            location='args',
        )
        issue_parser.add_argument(
            'notified_user_ids',
            type=int,
            action='append',
            location='args',
        )

        args = issue_parser.parse_args()
        sentry_issue = request.json
        issue_adapter = IssueAdapter(sentry_issue)

        payload = {
            'projectId': project_key_to_id(
                args.api_key, space_key, project_key),
            'summary': issue_adapter.summary,
            'issueTypeId': args.issue_type_id,
            'priorityId': args.priority_id,
            'categoryId[]': args.category_ids,
            'notifiedUserId[]': args.notified_user_ids,
            'description': issue_adapter.description,
        }
        result = CreateIssue(space_key, args.api_key).send(payload=payload)
        return dict(result=result)


api.add_resource(
    Index,
    '/',
)
api.add_resource(
    Project,
    '/<string:space_key>',
)
api.add_resource(
    Priority,
    '/<string:space_key>/priority',
)
api.add_resource(
    Category,
    '/<string:space_key>/<string:project_key>/category',
)
api.add_resource(
    IssueType,
    '/<string:space_key>/<string:project_key>/issuetype',
)
api.add_resource(
    User,
    '/<string:space_key>/<string:project_key>/user',
)
api.add_resource(
    Issue,
    '/<string:space_key>/<string:project_key>/issue',
)
