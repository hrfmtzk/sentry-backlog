from flask import Flask

from . import (
    index,
    api,
)


app = Flask(
    __name__,
    static_folder='../statics',
)


app.register_blueprint(index.app)
app.register_blueprint(api.app)
