from jinja2 import (
    Environment,
    FileSystemLoader,
)


class IssueAdapter:

    SUMMARY_MAX_LENGTH = 80

    def __init__(self, sentry_issue):
        self.sentry_issue = sentry_issue
        self.env = Environment(
            loader=FileSystemLoader('sentry_backlog/templates'),
        )

    @property
    def summary(self):
        first_line = self.sentry_issue['message'].split('\n')[0]

        if len(first_line) > 80:
            first_line = first_line[:77] + '...'

        return first_line

    @property
    def description(self):
        namespace = self.sentry_issue.copy()
        namespace.update(dict(
            exception=self.exception,
        ))
        tpl = self.env.get_template('backlog_issue_description.wiki')

        return tpl.render(namespace)

    @property
    def exception(self):
        interface_exception = self.sentry_issue['event'].get(
            'sentry.interfaces.Exception'
        )
        if interface_exception is None:
            return None

        messages = []

        def indent(n):
            return ' ' * 4 * n

        for value in interface_exception['values']:
            stacktrace = []

            first_line = ': '.join(filter(None, [
                value.get(key) for key in ['type', 'value']
            ]))
            stacktrace.append(first_line)

            frames = value['stacktrace'].get('frames', [])
            for frame in frames:
                stacktrace.append(
                    indent(1) + 'File "{filename}", line {lineno}, in {module}'
                    .format(**frame)
                )
                if 'context_line' in frame:
                    stacktrace.append(
                        indent(2) + frame['context_line'].strip()
                    )
            messages.extend(stacktrace)

        return '\n'.join(messages)
