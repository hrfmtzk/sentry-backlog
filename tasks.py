import json
import os

from invoke import task

from sentry_backlog.adapters.backlog import IssueAdapter


@task
def render_issue(ctx, filepath):
    issue = json.load(open(filepath, 'r'))
    ia = IssueAdapter(issue)

    print('#### Summary ####')
    print(ia.summary)

    print('#### Description ####')
    print(ia.description)


@task
def build_frontend(ctx, production=False):
    ctx.run(
        'cd frontend; npm run build:{0}'.format(
            'pro' if production else 'dev'
        ),
        pty=True,
    )


@task
def compile_requirements(ctx, production=True):
    target = os.path.join(
        'requirements',
        'production.in' if production else 'development.in'
    )
    ctx.run('pip-compile -o requirements.txt {0}'.format(target))
