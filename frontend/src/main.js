import Vue from 'vue'
import App from './components/App.vue'
import * as style from './main.scss'

Vue.directive('mdl', {
  bind: function (el) {
    componentHandler.upgradeElement(el)
  }
})

new Vue({
  el: '#app',
  render: h => h(App)
})
